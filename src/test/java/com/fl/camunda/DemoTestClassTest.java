package com.fl.camunda;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import com.fl.camunda.services.DemoTestClass;

@RunWith(SpringRunner.class)
public class DemoTestClassTest {

	@Test
	public void whenFindByName_thenReturnEmployee() {
		DemoTestClass testClass= new DemoTestClass();
		int result=testClass.add(5);
		assertThat(result).isEqualTo(10);
	}
}
