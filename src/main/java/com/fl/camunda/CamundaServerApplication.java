package com.fl.camunda;

import org.camunda.bpm.spring.boot.starter.annotation.EnableProcessApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fl.camunda.services.KafkaaConsumer;


@SpringBootApplication
@EnableProcessApplication
@ComponentScan(basePackages="com.fl.camunda.services")
@EntityScan( basePackages = {"com.fl.camunda.services"} )
@EnableJpaRepositories(basePackages="com.fl.camunda.services.repositories")
public class CamundaServerApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(CamundaServerApplication.class, args);
		//KafkaaConsumer kafkaaConsumer = new KafkaaConsumer();
		//kafkaaConsumer.runConsumer();
	}

}
