package com.fl.camunda.services;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class TestBpmnService implements JavaDelegate{

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		System.out.println(execution.getCurrentActivityName() +"  done !!!!!");
		System.out.println("variables : "+execution.getVariableNames());
	}

}
