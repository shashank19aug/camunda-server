package com.fl.camunda.services;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Properties;

import org.python.util.PythonInterpreter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component 
public class PythonService implements InitializingBean{
	private static final Logger LOGGER =LoggerFactory.getLogger(PythonService.class);
	private PythonInterpreter pyInterpreter = null;
	
	@Override
	public void afterPropertiesSet() throws Exception {
		LOGGER.debug("HI !!!!!!!!!!!!!!!!!!!!!");
		Properties props = new Properties();
		//props.put("python.home","path to the Lib folder from jython source jar");
		props.put("python.console.encoding", "UTF-8"); // Used to prevent: console: Failed to install '': java.nio.charset.UnsupportedCharsetException: cp0.
		//props.put("python.security.respectJavaAccessibility", "false"); //don't respect java accessibility, so that we can access protected members on subclasses
		props.put("python.home","/usr/local/bin/python");
		props.put("python.import.site","false");
		Properties preprops = System.getProperties();
		PythonInterpreter.initialize(preprops, props, new String[0]);
		pyInterpreter = new PythonInterpreter();	
		
	}
	

	public void callPyhtonService( String fileName) {
		
        try{
            ProcessBuilder pb = new ProcessBuilder("/usr/local/bin/python",
					fileName);
            Process p = pb.start();

            String line = "";
            System.out.println("Running Python starts: " + line);
            int exitCode = p.waitFor();
            System.out.println("Exit Code : "+exitCode);
            BufferedReader bfr = null;
            if(exitCode == 0)
            	bfr = new BufferedReader(new InputStreamReader(p.getInputStream()));
            else
            	bfr = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            
            line = bfr.readLine();
            System.out.println("First Line: " + line);
            while ((line = bfr.readLine()) != null){
                System.out.println("Python Output: " + line);
            }

        }catch(Exception e){
        	System.out.println(e);
        }
	}

}
