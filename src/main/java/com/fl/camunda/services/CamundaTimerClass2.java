package com.fl.camunda.services;

import java.util.concurrent.atomic.AtomicInteger;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class CamundaTimerClass2 implements JavaDelegate {
 AtomicInteger counter= new AtomicInteger();
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		int incCounter=counter.getAndIncrement();
		System.out.println("camunda 2 counter  :" +execution.getVariable("uuid"));
		//System.out.println("variableValues : "+execution.getVariables().containsKey("file_data")+"  :: "+((byte[])execution.getVariable("file_data")).length);
		long start = System.currentTimeMillis();
		long end = start + 5*1000; // 60 seconds * 1000 ms/sec
		while (System.currentTimeMillis() < end)
		{
		    // run
		}
		//System.out.println(" executed !!!!!"+"camunda 2 counter  :" +execution.getCurrentActivityId());

	}


}
