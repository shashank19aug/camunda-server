package com.fl.camunda.services;

import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fl.camunda.services.repositories.SpikyChannelRepo;

@Service
public class KafkaaConsumer {

	@Autowired
	SpikyChannelRepo spikyChannelRepo;
	public  void runConsumer() {
		Consumer<String, String> consumer =createConsumer();
		int noMessageFound = 0;
		while (true) {
			ConsumerRecords<String, String> consumerRecords = consumer.poll(1000);
			// 1000 is the time in milliseconds consumer will wait if no record is found at broker.
			if (consumerRecords.count() == 0) {
				noMessageFound++;
				if (noMessageFound > IKafkaConstants.MAX_NO_MESSAGE_FOUND_COUNT)
					// If no message found count is reached to threshold exit loop.  
					break;
				else
					continue;
			}
			//print each record. 

			consumerRecords.forEach(record -> {
				String recordValue1=record.value();
				//System.out.println(recordValue1);
				Integer indexOfStreamer=recordValue1.indexOf("Streamer");
				if(-1 !=indexOfStreamer) {
					String recordValue=recordValue1.substring(indexOfStreamer,recordValue1.length());
					Integer indexOfOpenBracket=recordValue.indexOf("[");
					if(-1 !=indexOfOpenBracket) {
						int indexOfCloseBracket=recordValue.indexOf("]");
						String spikyChannels=recordValue.substring(indexOfOpenBracket+1, indexOfCloseBracket);
						System.out.println(spikyChannels);
						int indexOfColon= recordValue.indexOf(":");
						String channelNum=recordValue.substring(indexOfColon-2,indexOfColon-1);
						System.out.println("channel number : "+ channelNum);
						
						/*SpikyChannel spikyChannel= new SpikyChannel();
						spikyChannel.setChannelNumber(channelNum);
						spikyChannel.setSpikyChannels(spikyChannels);
						spikyChannelRepo.save(spikyChannel);*/
					}
					
				}
			});
			// commits the offset of record to broker. 
			consumer.commitAsync();
		}
		consumer.close();
	}

	 /*static*/ Consumer<String, String> createConsumer() {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, IKafkaConstants.KAFKA_BROKERS);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, IKafkaConstants.GROUP_ID_CONFIG);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, IKafkaConstants.MAX_POLL_RECORDS);
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, IKafkaConstants.OFFSET_RESET_EARLIER);
		final Consumer<String, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Collections.singletonList(IKafkaConstants.TOPIC_NAME));
		return consumer;
	}
}
