package com.fl.camunda.services;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CallPythonBpmnService implements JavaDelegate {

	@Autowired
	private PythonService pythonService;
	
	@Override
	public void execute(DelegateExecution execution) throws Exception {
		System.out.println("variableValues : "+execution.getVariables());
		String pythonFileLoc=execution.getVariable("python_file_loc").toString();
		pythonService.callPyhtonService(pythonFileLoc);
		System.out.println("pythonFile executed !!!!!");

	}

}
