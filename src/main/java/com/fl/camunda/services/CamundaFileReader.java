package com.fl.camunda.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component
public class CamundaFileReader implements JavaDelegate {
	public void execute(DelegateExecution execution) throws Exception {
		UUID uuid = UUID.randomUUID(); 
		System.out.println("file reader counter "+uuid);
		//System.out.println("variableValues : "+execution.getVariables());
		execution.setVariable("uuid", uuid);
		execution.setVariable("file_data",readFileToByteArray(new File("/Users/shanky/Documents/work/documents/test.txt")));
		//System.out.println(" executed !!!!!"+" file reader counter "+execution.getProcessInstanceId());

	}
	
	 private  byte[] readFileToByteArray(File file){
	        FileInputStream fis = null;
	        // Creating a byte array using the length of the file
	        // file.length returns long which is cast to int
	        byte[] bArray = new byte[(int) file.length()];
	        try{
	            fis = new FileInputStream(file);
	            fis.read(bArray);
	            fis.close();        
	            
	        }catch(IOException ioExp){
	            ioExp.printStackTrace();
	        }
	        return bArray;
	    }

}
