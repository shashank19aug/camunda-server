package com.fl.camunda.services;

import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/camunda")
public class CamundaController {


	  @Autowired
	  private RuntimeService runtimeService;
	  
	@RequestMapping(value = "/invoke/{processId}", method = RequestMethod.GET)
	public String getAuthenticationByLogin(@PathVariable String processId) {
		runtimeService.startProcessInstanceByKey(processId);
		return "done !!";

	}
	
}
