package com.fl.camunda.services;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="spiky_channel")
public class SpikyChannel implements Serializable{

	@Id 
	@GeneratedValue(strategy = GenerationType.AUTO)
	 Long id;
	
	String channelNumber;
	String spikyChannels;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getChannelNumber() {
		return channelNumber;
	}
	public void setChannelNumber(String channelNumber) {
		this.channelNumber = channelNumber;
	}
	public String getSpikyChannels() {
		return spikyChannels;
	}
	public void setSpikyChannels(String spikyChannels) {
		this.spikyChannels = spikyChannels;
	}
	
}
