package com.fl.camunda.services;

import java.util.Random;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class QCValidateBpmnService implements JavaDelegate{

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		Random randy= new Random();
		System.out.println(execution.getVariableNames());
		execution.setVariable("isQcOk", randy.nextBoolean());
		System.out.println(execution.getVariableNames());
}

}
