package com.fl.camunda.services.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

import com.fl.camunda.services.SpikyChannel;

@Component
public interface SpikyChannelRepo  extends CrudRepository<SpikyChannel, Long> {

	
}



