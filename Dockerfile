FROM openjdk:11
ADD target/camunda-server-0.0.1-SNAPSHOT.jar camunda-server-0.0.1-SNAPSHOT.jar
EXPOSE 9090
ENTRYPOINT ["java", "-jar", "camunda-server-0.0.1-SNAPSHOT.jar"]